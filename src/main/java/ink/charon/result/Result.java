package ink.charon.result;

/**
 * @author Administrator
 * @version 1.0
 * @className Result
 * @description 官方定义的结果结构体
 * @date 2021/12/29 15:30
 */
public class Result {

    private Boolean unchange;
    private Boolean reject;
    private String reject_reason;

    public Result() {
    }

    public Result(Boolean unchange, Boolean reject, String reject_reason) {
        this.unchange = unchange;
        this.reject = reject;
        this.reject_reason = reject_reason;
    }

    public static Result success() {
        return new Result(true, false, null);
    }
    public static Result error(String msg) {
        return new Result(false, true, msg);
    }

    public Boolean getUnchange() {
        return unchange;
    }

    public void setUnchange(Boolean unchange) {
        this.unchange = unchange;
    }

    public Boolean getReject() {
        return reject;
    }

    public void setReject(Boolean reject) {
        this.reject = reject;
    }

    public String getReject_reason() {
        return reject_reason;
    }

    public void setReject_reason(String reject_reason) {
        this.reject_reason = reject_reason;
    }
}
