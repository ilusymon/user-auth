package ink.charon.controller;

import cn.hutool.json.JSONUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ink.charon.result.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author Administrator
 * @version 1.0
 * @className HandlerController
 * @description test
 * @date 2021/12/29 14:24
 */
@RestController
@CrossOrigin
public class HandlerController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping("/handler")
    public Result handler(@RequestBody Map<String, Object> map, HttpServletRequest request) {
        String version = (String) map.get("version");
        String option = (String) map.get("op");
        if ("Login".equals(option)) {
            try {
                Map<String, Object> content = (Map<String, Object>) map.get("content");
                Map<String, Object> metas = (Map<String, Object>) content.get("metas");
                String user = (String) content.get("user");
                String token = (String) metas.get("token");
                if ("test".equals(user) && "jA0(bK0~aJ1!cI5@mA5@oA7@aD1!lA".equals(token)) {
                    logger.info("A user is trying to login:" + user + "-->" + token + "(success)");
                    return Result.success();
                } else {
                    logger.error("A user is trying to login:" + user + "-->" + token + "(fail)");
                    return Result.error("Auth fail!");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return Result.success();
    }

}
